#!/usr/bin/env bash

# @description
#    Used to output messages in a consistent way
#    Prints an info message with green text to stdout and returns 0
cmnInfo() {

  {
    {
      local message &&
        local calling_function
    } || cmnFail "Variable declaration phase failed."
  } &&
  {
    {
      { { [[ -n "$1" ]] && message="$1"; } || cmnFail "Failed to pass a message to print."; } &&
        # default to "shell" if not called by a function
        calling_function="${FUNCNAME[1]:-shell}"
    } || cmnFail "Variable assignment phase failed."
  } &&
  {
    {
      { hash tput &> /dev/null || cmnWarn "Optional util (tput) not found in path."; } &&
        # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
        # 2 sets text color to green
        tput -T xterm-color setaf 2 2> /dev/null
        echo -e "\n[INFO] $calling_function | $message" &&
        # sgr0 removes all formatting
        tput -T xterm-color sgr0 2> /dev/null &&
        return 0
    } || cmnFail "Function execution phase failed."
  }

}

# @description
#    Used to output messages in a consistent way
#    Prints a warning message with yellow text for non-fatal errors to stderr and returns 0
cmnWarn() {

  {
    {
      local message &&
        local calling_function
    } || cmnFail "Variable declaration phase failed."
  } &&
  {
    {
      { { [[ -n "$1" ]] && message="$1"; } || cmnFail "Failed to pass a message to print."; } &&
        # default to "shell" if not called by a function
        calling_function="${FUNCNAME[1]:-shell}"
    } || cmnFail "Variable assignment phase failed."
  } &&
  {
    {
      hash tput &> /dev/null || cmnWarn "Optional util (tput) not found in path." &&
        # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
        # 3 sets text color to yellow
        tput -T xterm-color setaf 3 2> /dev/null &&
        echo -e "\n[WARN] $calling_function | $message" 1>&2 &&
        # sgr0 removes all formatting
        tput -T xterm-color sgr0 2> /dev/null &&
        return 0
    } || cmnFail "Function execution phase failed."
  }

}

# @description
#    Used to output messages in a consistent way
#    Prints a failure message with red text for fatal errors to stderr and returns 1
cmnFail() {

  {
    {
      local message &&
        local calling_function
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Variable declaration phase failed.\n" 1>&2
      return 1
    }
  } &&
  {
    {
      { { [[ -n "$1" ]] && message="$1"; } || cmnFail "Failed to pass a message to print."; } &&
        calling_function="${FUNCNAME[1]:-shell}" # default to "shell" if not called by a function
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Variable assignment phase failed.\n" 1>&2
      return 1
    }
  } &&
  {
    {
      hash tput &> /dev/null || cmnWarn "Optional util (tput) not found in path." &&
        # Why "xterm-color" and not "xterm" you ask? https://github.com/rburns/ansi-to-html/issues/25
        # 1 sets text color to red
        tput -T xterm-color setaf 1 2> /dev/null &&
        echo -e "\n[FAIL] $calling_function | ${message}\n" 1>&2 &&
        # sgr0 removes all formatting
        tput -T xterm-color sgr0 2> /dev/null &&
        return 1
    } || {
      echo -e "\n[FAIL] ${FUNCNAME[0]} | Function execution phase failed.\n" 1>&2
      return 1
    }
  }

}

# @description
#    usage message for the script
msUsage() {

  # Number of positional arguments must be 0 or 4 (all or none)
  if [[ "$#" == "0" ]]; then
    echo
    echo "  Description:"
    echo "  ${0##*/} is a helper script used to rsync mirror data from an external storage device to a webserver's web-facing directories"
    echo 
    echo "  Usage:"
    echo "  $0 [server_mirror_tld] [storage_mirror_tld] [storage_device_name] ['url_one url_two etc'] | null"
    echo
    echo "  Positional Arguments:"
    echo "    1: Server's mirror top-level directory (absolute path on server) that data should be synced TO (i.e. /var/www/html)"
    echo "    2: Storage's mirror top-level directory (relative path on storage device) that data should be synced FROM (i.e. mirror-data/var/www/html/)"
    echo "    3: Name of the storage device to mount (i.e. /dev/sdc) to '/mnt/\$device_name' (i.e. /mnt/dev/sdc), if not already mounted elsewhere"
    echo "    4: Quoted, space-seperated list of URLs to check HTTP status codes of before and after sync process"
    echo
    echo "  Note: The use of positional arguments is optional; if not used, you will be prompted for inputs."
    echo
    return 0
  fi
  if [[ "$#" != "4" ]]; then
    cmnFail "Number of positional arguments provided to the script must be zero or four (all or none)." || return 1
  fi

}

# @description
#    parses options passed to script
msParseOptions() {

  # If $1 is empty or not a valid directory, prompt user for server mirror top-level directory
  if [[ -n "$1" ]] && [[ -d "$1" ]]; then
    server_mirror_tld="$1"
    export server_mirror_tld
  else
    read -rp "Mirror top-level directory (on server): " server_mirror_tld
  fi
  # If server_mirror_tld still not a valid directory, fail; otherwise, pass.
  if [[ -d "$server_mirror_tld" ]]; then
    cmnInfo "Server's mirror top-level directory ($server_mirror_tld) has been set."
  else
    cmnFail "Failed to set a valid server mirror top-level directory." || return 1
  fi

  # If $2 is empty prompt user for storage mirror top-level directory
  if [[ -n "$2" ]]; then
    storage_mirror_tld="$2"
    export storage_mirror_tld
  else
    read -rp "Mirror top-level directory (on storage device): " storage_mirror_tld
  fi
  # If storage_mirror_tld still not a valid directory, fail; otherwise, pass.
  if [[ -n "$storage_mirror_tld" ]]; then
    cmnInfo "Storage device's mirror top-level directory ($storage_mirror_tld) has been set."
  else
    cmnFail "Failed to set a valid storage device's mirror top-level directory." || return 1
  fi

  # If $3 is empty, does not start with "/dev", or does not exist on file system, prompt user for storage device location
  if [[ -n "$3" ]] && [[ -e "$3" ]] && [[ "$3" =~ ^/dev/.*$ ]]; then
    storage_device="$3"
    export storage_device
  else
    cmnInfo "Please review the device list below and find the name (/dev/*) of the storage device containing your mirror data." &&
    lsblk &&
    read -rp "Location of storage device (/dev/*): " storage_device
  fi
  # If storage_device still not a valid value, fail; otherwise, pass.
  if [[ -n "$storage_device" ]] && [[ -e "$storage_device" ]] && [[ "$storage_device" =~ ^/dev/.* ]]; then
    cmnInfo "Storage device location ($storage_device) has been set."
  else
    cmnFail "Failed to set a valid storage device location." || return 1
  fi

  # If $4 is empty, prompt user for url list
  if [[ -n "$4" ]]; then
    url_list="$4"
    export url_list
  else
    read -rp "Space-seperated list of URLs to check status of (i.e. 'ubuntu.org centos.org'): " url_list
  fi
  # If url_list still not a valid value, fail; otherwise, pass.
  if [[ -n "$url_list" ]]; then
    cmnInfo "URL list ($url_list) has been set."
  else
    cmnFail "Failed to set a valid URL list." || return 1
  fi

}

# @description
#    Mounts external storage device
msMountStorageDevice() {

  local storage_device="${1##*/}"
  local storage_mirror_tld="$2"

  cmnInfo "Mounting storage device ($storage_device) to /mnt/$storage_device"
  mkdir -p "/mnt/$storage_device" &&
  { umount "/dev/$storage_device" || true; } &&
  mount "/dev/$storage_device" "/mnt/$storage_device"
  if [[ ! -d "/mnt/${storage_device}/${storage_mirror_tld}" ]]; then
    cmnFail "Storage mirror top-level directory (/mnt/${storage_device}/$storage_mirror_tld) does not exist; exiting." || return 1
  fi

}

# @description
#    rsyncs data into mirror directory
msRsyncData() {

  local server_mirror_tld="$1"
  local storage_mirror_tld="$2"
  local storage_device="$3"

  cmnInfo "Fixing any files in storage mirror top-level directory (/mnt/${storage_device##*/}/${storage_mirror_tld}/) with incorrect file ownerships..."
  nice -n -18\
    find \
      "/mnt/${storage_device##*/}/${storage_mirror_tld}/" \
      \( -type d -o -type f \) \
      -a \( \! -user apache -o \! -group apache \) \
      -a -printf 'before chown: file=%p\t user=%u\t group=%g\n' \
      -a -exec chown apache:apache '{}' \; || cmnWarn "Failed to update all incorrect file ownerships."
  cmnInfo "Fixing any files in storage mirror top-level directory (/mnt/${storage_device##*/}/${storage_mirror_tld}/) with incorrect file permissions..."
  nice -n -18 \
    find \
      "/mnt/${storage_device##*/}/${storage_mirror_tld}/" \
      \( -type f -o -type d \) \
      -a -perm /o=w \
      -a -printf 'before chmod: file=%p\t mode=%M\n' \
      -a -exec chmod o-w '{}' \; || cmnWarn "Failed to update all incorrect file permissions."  
  cmnInfo "Running: nice -n -18 rsync -avP \"/mnt/${storage_device##*/}/${storage_mirror_tld}/\*\" \"${server_mirror_tld}/\""
  nice -n -18 \
    rsync \
      -avP \
      "/mnt/${storage_device##*/}/${storage_mirror_tld}/"* \
      "${server_mirror_tld}/" || cmnWarn "Rsync exited with errors, please check output."
  cmnInfo "Rsync complete; please carefully check output to ensure expected results."

}

# @description
#    smoke test mirror vhost entry
msSmokeTestURL() {

  local url_list="$1"

  for url in $url_list; do
    cmnInfo "Determining status of URL ($url)..."
    # Perform HEAD request, timeout after 5 seconds, ignore SSL errors
    curl -m 5 -Ik "$url" || cmnWarn "Failed to determine status of URL."
  done

}

# @description
#    Main function; entry point function where execution starts
main() {

  if [[ "$EUID" != "0" ]]; then
    cmnFail "Must be superuser or root; exiting." || return 1
  fi
  msUsage "$@" && 
  msParseOptions "$@" &&
  msMountStorageDevice "$storage_device" "$storage_mirror_tld" &&
  msSmokeTestURL "$url_list" &&
  msRsyncData "$server_mirror_tld" "$storage_mirror_tld" "$storage_device" &&
  msSmokeTestURL "$url_list"

}

main "$@" || exit 1
